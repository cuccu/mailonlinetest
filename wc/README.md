# wc

The application works using pattern matching.
The design is very basic, one main class that parses parameter and
calls a specific function on the core namespace.
A very basic utility for text formatting is provided in ui namespace.

## Usage
Run tests with lein test

Build the jar with lein uberjar

Run the application with:
java -jar wc-0.0.1-SNAPSHOT-standalone.jar <file.txt>
java -jar wc-0.0.1-SNAPSHOT-standalone.jar --all <file.txt>
java -jar wc-0.0.1-SNAPSHOT-standalone.jar --frequencies <file.txt>
