(ns wc.core-test
  (:require [clojure.test :refer :all])
  (:require [clojure.java.io :as io])
  (:use wc.core))

(defn slurp-res [file-name]
  (-> file-name io/resource io/file slurp))

(deftest word-count-test
  (is (= 5 (word-count (slurp-res "one-line.txt"))))
  (is (= 0 (word-count (slurp-res "empty-file.txt"))))
  (is (= 11 (word-count (slurp-res "empty-lines.txt"))))
  (is (= 0 (word-count (slurp-res "no-words.txt"))))
  (is (= 11 (word-count (slurp-res "more-lines.txt")))))

(deftest metric-count-test
  (is (= {:lines 1 :words 5 :chars 21} (metric-count (slurp-res "one-line.txt"))))
  (is (= {:lines 1 :words 0 :chars 0} (metric-count (slurp-res "empty-file.txt"))))
  (is (= {:lines 4 :words 11 :chars 51} (metric-count (slurp-res "empty-lines.txt"))))
  (is (= {:lines 3 :words 0 :chars 0} (metric-count (slurp-res "no-words.txt"))))
  (is (= {:lines 2 :words 11 :chars 51} (metric-count (slurp-res "more-lines.txt")))))

(deftest freq-count-test
  (is (= {"Very" 1, "nice" 1, "ad" 1, "hoc" 1, "counting" 1} (freq-count (slurp-res "one-line.txt"))))
  (is (= {} (freq-count (slurp-res "empty-file.txt"))))
  (is (= {"Very" 2, "nice" 3, "ad" 1, "hoc" 1, "counting" 2, "Really" 1, "very" 1} (freq-count (slurp-res "empty-lines.txt"))))
  (is (= {} (freq-count (slurp-res "no-words.txt"))))
  (is (= {"Very" 2, "nice" 3, "ad" 1, "hoc" 1, "counting" 2, "Really" 1, "very" 1} (freq-count (slurp-res "more-lines.txt")))))
  
