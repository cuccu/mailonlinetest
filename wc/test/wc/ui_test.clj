(ns wc.ui-test
  (:require [clojure.test :refer :all])
  (:use wc.ui))

(deftest format-map-test
  (is (= 
         "lines: 3, words: 18, chars: 99" 
         (format-map {:lines 3 :words 18 :chars 99})))
  (is (= 
         "Very: 2, nice: 3, ad: 1, hoc: 1, counting: 2, Really: 1, very: 1" 
         (format-map {"Very" 2, "nice" 3, "ad" 1, "hoc" 1, "counting" 2, "Really" 1, "very" 1}))))






