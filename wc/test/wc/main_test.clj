(ns wc.main-test
  (:require [clojure.test :refer :all])
  (:use wc.main))

(deftest main-test
  (with-redefs [wc.core/word-count (fn [s] (str "word-count: " s))
                wc.core/freq-count (fn [s] (str "freq-count: " s))
                wc.core/metric-count (fn [s] (str "metric-count: " s))
                wc.ui/format-map (fn [m] m)]
    (is (= "Please specify a filename\n" (with-out-str (-main))))
    (is (= "word-count: Very nice ad-hoc counting.\n" (with-out-str (-main "resources/one-line.txt"))))
    (is (= "freq-count: Very nice ad-hoc counting.\n" (with-out-str (-main "--frequencies" "resources/one-line.txt"))))
    (is (= "metric-count: Very nice ad-hoc counting.\n" (with-out-str (-main "--all" "resources/one-line.txt"))))
    (is (= "Please use --all or --frequencies\n" (with-out-str (-main "--something" "resources/one-line.txt"))))))





