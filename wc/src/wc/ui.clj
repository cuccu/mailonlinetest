(ns wc.ui
  (:require [clojure.string :refer [join]]))

(defn format-map [m]
  (join 
    ", " 
    (reduce 
      #(concat %1 [(str (name (first %2)) ": " (second %2))]) 
      [] 
      m)))

