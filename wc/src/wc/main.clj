(ns wc.main
  (:gen-class)
  (:use wc.ui)
  (:use wc.core))

(defn usage []
  (println "Usage: please specify a filename"))

(defn -main [& args]
  (cond 
    (= 0 (count args))
    (println "Please specify a filename")
    (= 1 (count args))
    (println (word-count (slurp (first args))))
    (and (= 2 (count args)) (= "--all" (first args)))
    (println (format-map (metric-count (slurp (second args)))))
    (and (= 2 (count args)) (= "--frequencies" (first args)))
    (println (format-map (freq-count (slurp (second args)))))
    :else (println "Please use --all or --frequencies")))

