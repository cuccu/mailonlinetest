(defn ^{:private true} quiz
  "Lot of stuff"
  {:inline (fn [x] `(str ~x))
   :inline-arities #{1}
   :private true}
  (^{:tag String}     
   [[x :as v]]      
   {:pre [(odd? x)]}        
   (apply str v))
  (^{:post [(pos? %)] :tag double}
    [^long a ^double b]
    (- a b))
  {:profile "cpu"})


(defn myfunc
  "some comment"
  {:inline (fn [x] `(str ~x))
   :inline-arities #{1}
   :private true}
  (^{:tag String} [x] (apply str x))
  (^{:tag Double} [x y] (apply str x y)))



(defn ^{:private true} quiz
  "Lot of stuff"
  (^{:tag String}     
   [[x :as v]]      
   {:pre [(odd? x)]}        
   (apply str v))
  (^{:post [(pos? %)] :tag double}
    [^long a ^double b]
    (- a b))
  {:profile "cpu"})
