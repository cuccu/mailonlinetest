(ns wc.core
  (:require [clojure.string :refer [split-lines]]))

(def pattern #"[a-zA-Z0-9]+")

(defn word-count [s]
  (count (re-seq pattern s)))

(defn metric-count [s]
  (let [words (re-seq pattern s)]
    {:lines (count (split-lines s))
     :words (count words)
     :chars (reduce #(+ %1 (count %2)) 0 words)}))

(defn freq-count [s]
  (frequencies (re-seq pattern s)))
    
